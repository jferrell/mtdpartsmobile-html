desc "Minify CSS and JS, then copy to appropriate directories"
task :mtd_minify do
	require 'jammit'
	Jammit.package!
end