require 'sinatra'
require_relative 'models/cart'
require_relative 'models/store'
require_relative 'models/service_provider'
require_relative 'models/order'
require_relative 'models/article'
require_relative 'models/products'
require_relative 'models/diagnose'

configure do
  enable :sessions
  disable :protection
  if ENV['RACK_ENV'] != "development"
    set :static_cache_control, [:public, :max_age => 31536000]
  end
end

before do
  @serve_minified = false
  if ENV['RACK_ENV'] != "development"
    expires 3000, :public, :must_revalidate
    @serve_minified = true
  end
  @cart = session[:cart]
  @cart_items = Cart.total_items(session[:cart])  
  @cart_amount = Cart.total_amount(session[:cart])
  @cart_subamount = @cart_amount - 3
end

get '/' do
  @page = 'home'
  erb :index, :layout => :layout
end

get '/find-parts' do
  @page = 'find-parts'
  erb :find_parts, :layout => :layout
end

post '/find-parts' do
  random = rand(9)
  if random == 0
    @page = 'find-parts'
    if !params[:part_number].nil?
      @part_not_found = true
    else
      @part_number_missing = true
    end
    erb :find_parts, :layout => :layout
  else  
    redirect "/products/detail/329744"
  end
end

get '/products' do
  @page = 'products'
  erb :products_browse, :layout => :layout
end

get '/products/detail/:id' do
  @page = 'products'
  @id = params[:id]
  @product = Products.get(@id)
  erb :products_detail, :layout => :layout
end

get '/products/search' do
  @page = 'products'
  random = rand(4)
  if random == 0
    @page_error = true
    erb :products_browse, :layout => :layout 
  else
    @products = Products.get_all();
    erb :products_results, :layout => :layout  
  end
end

get '/products/register' do
  @page = 'products'
  erb :products_register, :layout => :layout
end

post '/products/register' do
  @page = 'products'
  @page_message = true
  erb :my_account_anon, :layout => :layout
end

get '/products/:category' do
  @page = 'products'
  @category_title = Products.get_category(params[:category])
  @products = Products.get_all();
  erb :products_list, :layout => :layout
end

get '/cart' do
  @page = 'show-cart'
  erb :index, :layout => :layout
end

get '/cart/add/error/:product_id' do
  @page = 'cart'
  @product_id = params[:product_id]
  @product = Products.get(@product_id)
  erb :cart_add_error, :layout => :layout
end

post '/cart/update' do
  @page = 'cart'
  session[:cart] = Cart.update_item(
                          session[:cart], 
                          params[:product_id],
                          params[:product_quantity]
                        )
  redirect to('/cart')
end

post '/cart/delete' do
  @page = 'cart'
  session[:cart] = Cart.delete_item(
                          session[:cart], 
                          params[:product_id]
                        )
  redirect to('/cart')
end

post '/cart' do
  random = rand(9)
  if random != 0
    @page = 'cart'
    session[:cart] = Cart.add_item(
                            session[:cart], 
                            params[:product_id],
                            params[:product_price], 
                            params[:product_quantity]
                          )
    redirect to('/cart')
  else
    redirect to ('/cart/add/error/' + params[:product_id])
  end 
end

post '/cart/promo' do
  @page = 'show-cart'
  @error = true
  erb :cart, :layout => :layout
end

get '/cart/checkout' do
  if logged_in?
    redirect to '/cart/checkout/shipping'
  end
  @page = 'cart'
  erb :cart_sign_in, :layout => :layout
end

post '/cart/checkout/signin' do
  redirect to('/cart/checkout/shipping')
end

get '/cart/checkout/shipping' do
  @page = 'cart'
  erb :cart_shipping, :layout => :layout
end

post '/cart/checkout/shipping' do
  redirect to('/cart/checkout/payment')
end

get '/cart/checkout/payment' do
  @page = 'cart'
  erb :cart_payment, :layout => :layout
end

post '/cart/checkout/payment' do
  redirect to('/cart/checkout/thanks')
end

get '/cart/checkout/thanks' do
  @page = 'cart'
  erb :cart_thanks, :layout => :layout
end

get '/forgot-password' do
  @page = 'sign-in'
  @from = params[:from]
  erb :forgot_password, :layout => :layout
end

post '/forgot-password' do
  if params[:email]
    @page_message = true
  end
  @page = 'sign-in'
  erb :forgot_password, :layout => :layout
end

get '/forgot-username' do
  @page = 'sign-in'
  @from = params[:from]
  erb :forgot_username, :layout => :layout
end

post '/forgot-username' do
  if params[:email]
    @page_message = true
  end
  @page = 'sign-in'
  erb :forgot_username, :layout => :layout
end

get '/change-password' do
  @page = 'change-password'
  erb :change_password, :layout => :layout
end

post '/change-password' do
  @page = 'change-password-success'
  erb :change_password_success, :layout => :layout
end

post '/login' do
  response.set_cookie("logged-in", true)
  if session[:return_to] && session[:return_to] != ''
    @return_url = session[:return_to]
    session[:return_to] = ''
    redirect to @return_url
  elsif params[:return_url] && params[:return_url] != ''
    redirect to params[:return_url]
  elsif request.referer && request.url != request.referer
    redirect to request.referer
  else
    redirect to '/'
  end
end

get '/logout' do
  request.cookies["logged-in"] = nil;
  response.set_cookie("logged-in", false)
  if request.referer && request.url != request.referer 
    redirect to request.referer
  else
    redirect to '/'
  end
end

get '/sign-in' do
  @page = 'sign-in'
  erb :sign_in, :layout => :layout
end

get '/my-account' do
  @page = 'my-account'
  if !logged_in?
    erb :my_account_anon, :layout => :layout
  else
    erb :my_account_loggedin, :layout => :layout
  end
end

get '/my-account/create' do
  @page = 'my-account'
  erb :my_account_create, :layout => :layout
end

post '/my-account/create' do
  if !logged_in?
    response.set_cookie("logged-in", true)
  end
  if session[:return_to] && session[:return_to] != ''
    @return_url = session[:return_to]
    session[:return_to] = ''
    redirect to @return_url
  else
    @page_message = true
    erb :my_account_loggedin, :layout => :layout
  end
end

get '/my-account/update' do
  if !logged_in?
    @page_message = true
    erb :sign_in, :layout => :layout
  else  
    erb :my_account_update, :layout => :layout
  end
end

post '/my-account/update' do
  @page_message_account_updated = true
  erb :my_account_loggedin, :layout => :layout
end

get '/my-products' do
  @page = 'my-products'
  if logged_in?
    erb :my_products_loggedin, :layout => :layout
  else
    session[:return_to] = '/my-products'
    erb :my_products_anon, :layout => :layout
  end
end

get '/my-products/detail' do
  @page = 'my-products'
  erb :my_products_detail, :layout => :layout
end

get '/my-products/add' do
  @page = 'my-products'
  erb :my_products_add, :layout => :layout
end

post '/my-products/add' do
  @page = 'my-products'
  @page_message_product_added = true
  erb :my_products_detail, :layout => :layout
end

post '/my-products/update' do
  @page = 'my-products'
  @page_message_product_updated = true
  erb :my_products_detail, :layout => :layout
end

get '/my-products/update' do
  @page = 'my-products'
  erb :my_products_update, :layout => :layout
end

post '/my-products/update' do
  @page = 'my-products'
  @page_message_product_updated = true
  erb :my_products_detail, :layout => :layout
end

get '/my-products/delete' do
  @page = 'my-products'
  erb :my_products_delete, :layout => :layout
end

post '/my-products/delete' do
  @page = 'my-products'
  @page_message_product_deleted = true
  erb :my_products_loggedin, :layout => :layout
end

get '/orders' do
  if logged_in?
    redirect to '/orders/list'
  end
  @page = 'orders'
  erb :order_status, :layout => :layout
end

get '/orders/details' do
  if logged_in?
    redirect to '/orders/list'
  end
  @page = 'orders'
  random = rand(4)
  if random == 0
    @page_error = true
    erb :order_status, :layout => :layout
  else
    erb :order_details, :layout => :layout
  end
end

get '/orders/view-details' do
  @page = 'orders'
  if params[:order_number].nil?
    @active_order = Order.get_order(0)
  else
    @active_order = Order.get_order(params[:order_number])
  end
  erb :order_view_details, :layout => :layout
end

get '/orders/list' do
  if !logged_in?
    redirect to '/orders'
  end
  @page = 'orders'
  @order_numbers = Order.orders
  erb :order_status_results, :layout => :layout
end

get '/orders/tracking' do
  if params[:order_number].nil?
    redirect to '/orders/list'
  end
  @page = 'orders'
  erb :order_tracking, :layout => :layout
end

get '/manuals' do
  erb :manuals, :layout => :layout
end

get '/manuals/decline-terms' do
  redirect to '/manuals'
end

get '/manuals/results' do
  @page = 'manuals'
  erb :manuals_results, :layout => :layout
end

post '/manuals/results' do
  @page = 'manuals'
  erb :manuals_results, :layout => :layout
end

get '/manuals/:id' do
  @page = 'manuals'
  @id = params[:id]
  if request.cookies["manuals-accepted-terms"] == 'true'
    erb :manuals_detail, :layout => :layout
  else
    erb :manuals_disclaimer, :layout => :layout
  end  
end

get '/service-locator' do
  erb :service_locator, :layout => :layout
end

get '/service-locator/results' do
  @page = 'owners'
  @lat = params[:latitude]
  @lon = params[:longitude]
  @zip = params[:zip]
  @radius = params[:radius]
  @service_providers = ServiceProvider.service_providers
  erb :service_results, :layout => :layout
end

get '/service-locator/map/:id' do
  @page = 'store'
  @lat = params[:latitude]
  @lon = params[:longitude]
  @zip = params[:zip]
  @radius = params[:radius]
  @service_provider = ServiceProvider.service_providers.select { |s| s[:id] === Integer(params[:id]) }[0]
  erb :service_map, :layout => :layout
end

get '/knowledge-center' do
  @page = 'knowledge-center'
  @categories = Article.categories
  erb :knowledge_center_landing, :layout => :layout  
end

get '/knowledge-center/search' do
  @page = 'knowledge-center'
  @categories = Article.categories
  random = rand(4)
  if random == 0
    @page_error = true
    erb :knowledge_center_landing, :layout => :layout 
  else
    erb :knowledge_center_results, :layout => :layout  
  end
end

get '/knowledge-center/:category' do
  @page = 'knowledge-center'
  @category = Article.categories[params[:category]]
  erb :knowledge_center_category, :layout => :layout  
end

get '/knowledge-center/:category/video-step-by-step' do
  @page = 'knowledge-center'
  @category = Article.categories[params[:category]]
  erb :knowledge_center_video_steps, :layout => :layout  
end

get '/knowledge-center/:category/article-step-by-step' do
  @page = 'knowledge-center'
  @category = Article.categories[params[:category]]
  erb :knowledge_center_article_steps, :layout => :layout  
end

get '/knowledge-center/:category/article-table' do
  @page = 'knowledge-center'
  @category = Article.categories[params[:category]]
  erb :knowledge_center_article_table, :layout => :layout  
end

get '/knowledge-center/:category/article' do
  @page = 'knowledge-center'
  @category = Article.categories[params[:category]]
  erb :knowledge_center_article, :layout => :layout  
end

get '/contact' do
  @page = 'contact'
  erb :contact_us, :layout => :layout
end

get '/about' do
  @page = 'about'
  erb :about, :layout => :layout
end

get '/privacy' do
  @page = 'privacy'
  erb :privacy_policy, :layout => :layout
end

get '/locate-model' do
  @page = 'locate-model'
  erb :locate_model, :layout => :layout
end

get '/diagnose-troubleshoot' do
  @page = 'diagnose'
  @categories = Diagnose.get_all_categories()
  erb :diagnose, :layout => :layout
end

get '/diagnose-troubleshoot/:category' do
  @page = 'diagnose'
  @category = Diagnose.get_category(params[:category])
  erb :diagnose_symptoms, :layout => :layout
end

get '/espot-ad' do
  @page = 'espot'
  erb :espot_ad, :layout => :layout
end

get '/404' do
  @page = '404'
  erb :error_404, :layout => :layout
end

get '/500' do
  @page = '500'
  erb :error_500, :layout => :layout
end

not_found do
  @page = '404'
  erb :error_404, :layout => :layout
end

error do
  @page = '500'
  erb :error_500, :layout => :layout
end

##################
# helper methods #
##################
def logged_in?
  return request.cookies["logged-in"] == 'true'
end
