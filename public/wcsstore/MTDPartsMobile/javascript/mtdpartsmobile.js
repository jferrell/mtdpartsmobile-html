var touch_event = (typeof Touch == "object") ? 'tap' : 'click';
var mtdparts = (typeof mtdparts !== 'undefined') ? mtdparts : {};
mtdparts.mobile = (typeof mtdparts.mobile !== 'undefined') ? mtdparts.mobile : {};

(function (mtdpm) {
	var duration = 500;	
	var isAndroidBrowser = (navigator.userAgent.indexOf('Android') > 0 && navigator.userAgent.indexOf('Chrome') < 0) ? true : false;
	if (isAndroidBrowser) $('html').addClass('android-browser');
	mtdpm.optimize = {
		init : function () {
			$('.lazy-load').each(mtdpm.optimize.lazyLoad);
			$(window).on('scroll', function () {
				// lazy load on scroll
				$('.lazy-load').each(mtdpm.optimize.lazyLoad);
			});
		},
		lazyLoad : function () {
			var el = this;
			var src;
			// make sure image is in viewport
			if(mtdpm.optimize.elementInViewport(el) || $(el).hasClass('slider-img') || $(el).hasClass('cart-lazyload')) {
				// check for retina display
				if( window.devicePixelRatio >= 1.5 ){
					src = $(el).data('img-hd');
				} else {
					src = $(el).data('img');
				}
				var ajax_cache = true;
				// do not cache ajax request on Android browser due to bug
				if (isAndroidBrowser) ajax_cache = false;
				$.ajax({
					url: src,
					cache: ajax_cache,
					success: function(data) {
						$(el).attr('src', src).animate({ opacity: 1 }, 400).removeClass('lazy-load');
					}
				});
			}
		},
		elementInViewport : function (el) {
		  var top = el.offsetTop;
		  var left = el.offsetLeft;
		  var width = el.offsetWidth;
		  var height = el.offsetHeight;
		  while(el.offsetParent) {
		    el = el.offsetParent;
		    top += el.offsetTop;
		    left += el.offsetLeft;
		  }
		  return (
		    top < (window.pageYOffset + window.innerHeight) &&
		    left < (window.pageXOffset + window.innerWidth) &&
		    (top + height) > window.pageYOffset 
		  );
		}
	}
	mtdpm.cart = {
		init : function() {
			$('.item-meta').on('click', mtdpm.cart.expandItem);
			$('.select-submit').on('change', mtdpm.cart.selectSubmit);
			$('#credit-card-number').on('keyup', mtdpm.cart.cardListener);
			$('.form-checkbox-shipping').on('click', mtdpm.cart.billingSame);
		},
		expandItem : function(e) {
			if (! $(this).parent().hasClass('expanded')) {
				$('.cart li').removeClass('expanded');
			}
			$(this).parent().toggleClass('expanded');
		},
		billingSame : function(e) {
			// set checkbox if click from label
			var check = $('input', this);
			if (e.target.type != 'checkbox') { 
				if (check.prop('checked')) {
					check.prop('checked', false);
				}else {
					check.prop('checked', true)
				}
			}	
			var required_fields = $('#cart-billing [data-valid]');
			if (!check.prop('checked')) { // add required prop to fields with data-valid attribute if unchecked
				required_fields.attr('required', '');
			}else { // remove required prop to fields with data-valid attribute if checked
				required_fields.removeAttr('required');
			}
			// toggle billing fields
			$('#cart-billing').toggle();
		},
		selectSubmit : function() {
			this.parentNode.submit();
		},
		cardListener : function() {
			var accepted_cards = $('.accepted-cards').find('li');
			var existing_match = $('.match');
			var card_type = mtdpm.cart.detectCardType($(this).attr('value').toString());
			if (card_type) {
				if (! existing_match.hasClass(card_type)) {
					accepted_cards.removeClass('match').removeClass('no-match');
					accepted_cards.each(function(el, index, xui) {
						if ($(this).hasClass(card_type)) {
							$(this).toggleClass('match');
						}else {
							$(this).toggleClass('no-match');		
						}
					});					
				}
			} else {
				accepted_cards.removeClass('match').removeClass('no-match');
			}
		},
		detectCardType : function(card_number) {
			card_number = card_number.replace(/\D/g, '');
			var cards = mtdpm.acceptedCards;
			for (var ci in cards) {
				if (cards.hasOwnProperty(ci)) {
					var c = cards[ci];
				    for(var pi = 0, pl = c.prefixes.length; pi < pl; ++pi) {
				    	if(c.prefixes.hasOwnProperty(pi)) {
				        	var p = c.prefixes[pi];
				          	if (new RegExp('^' + p.toString()).test(card_number)) {
				            	return ci;
										}
				        }
				    }
				}
			}
			return false;
		}
	}
	mtdpm.acceptedCards = {
		'visa' : {
			prefixes : [4]
		},
		'mastercard' : {
			prefixes : [51, 52, 53, 54, 55]
		},
		'americanexpress' : {
			prefixes : [34, 37]
		},
		'discover' : {
			prefixes : [6011, 62, 64, 65]
		}	
	};
	mtdpm.validate = {
		init : function() {
			mtdpm.validate.userErrors = [];
			$('form.validate').on('submit', mtdpm.validate.perform);
			$('form.validate input').on('invalid', mtdpm.validate.catchInvalid);
			$('select.seed-input').on('change', mtdpm.validate.seedInputs);
		},
		perform : function(e) {	
			var fields = [];
			$(this).find('*').each(function () {
				mtdpm.validate.hideError(this);
				if (this.hasAttribute('required')) {
					fields.push(this);
				}
			});
			var errors = 0;
			mtdpm.validate.userErrors = [];
			$(fields).each(function(i) {
				if (! mtdpm.validate.hasValue(this)) {
					mtdpm.validate.showError(this);
					errors++;
					e.preventDefault();
				}else {
					mtdpm.validate.hideError(this);
				}
			});
			if (errors > 0 || (mtdpm.validate.hascheckValidity() && !this.checkValidity())) {
				if (mtdpm.validate.hascheckValidity()) {
					this.checkValidity();
				}
				var user_errors = $('.user-errors', $(this));
				var user_error_list = $('ul', user_errors);
				var num_errors = mtdpm.validate.userErrors.length;
				user_error_list.html('');
				for (var i=0; i < num_errors; i++) {
					user_error_list.append('<li>' + mtdpm.validate.userErrors[i] + '</li>');
				}
				if (num_errors > 0) {
					user_errors.addClass('show');
					e.preventDefault();
				}else {
					user_errors.removeClass('show');
				}				
			}
			if ($('.user-pwd-match').size() > 0) {
				if (! mtdpm.validate.passwordMatch()) {
					$('.user-pwd-match').addClass('show');	
					e.preventDefault();				
				} else {
					$('.user-pwd-match').removeClass('show');
				}
			}
			if ($('.user-email-match').size() > 0) {
				if (! mtdpm.validate.emailMatch()) {
					$('.user-email-match').addClass('show');	
					e.preventDefault();				
				} else {
					$('.user-email-match').removeClass('show');	
				}
			}	
			if ($(this).hasClass('find-store-form')) {
				if ((! $('#location').prop('checked')) && ($('#zip').val().length == 0)) {
					$('#error-zip-code').addClass('show');
					e.preventDefault();
				}
			}		
		},
		hascheckValidity : function() {
		    return (typeof document.createElement( 'input' ).checkValidity === 'function');
		},
		passwordMatch : function() {
			return $('#password').attr('value').toString() == $('#password-confirm').attr('value').toString();
		},
		emailMatch : function() {
			return $('#email').attr('value').toString() == $('#email-confirm').attr('value').toString();
		},
		hasValue : function(el) {
			var type = el.nodeName.toLowerCase();
			switch(type) {
				case 'select':
					return el.options[el.selectedIndex].value != '';
					break;
				default:
					if ($(el).attr('type').toString() == 'checkbox') {
						return el.checked;
					}else {
						return $(el).attr('value').toString().length > 0;	
					}					
					break;
			}
		},
		catchInvalid : function (e) {
			var el = this;
			var field_type = $(el).attr('type');
			var msg = el.validationMessage;
			var required_str = " is required";
			var invalid_str = " is invalid";
			if(msg != 'value missing' && $(el).val() != '') {
				switch(field_type) {
					case 'email':
						msg = $($('label[for="' + $(el).attr('id') + '"]')).html().replace('*','').replace(':','') + invalid_str + ' (must contain an "@" and ".")';
						mtdpm.validate.userErrors.push(msg);
						break;
					default:
						msg = $($('label[for="' + $(el).attr('id') + '"]')).html().replace('*','').replace(':','') + invalid_str;
						mtdpm.validate.userErrors.push(msg);
						break;
				}
				$('#' + $(el).attr('data-valid')).html(msg);
				mtdpm.validate.showError(el);
			}else {	
				if ($(el).hasClass('hidden-phone')) {
					mtdpm.validate.userErrors.push('Phone' + required_str);
				}else {
					mtdpm.validate.userErrors.push($(el).prev().html().replace('*','').replace(':','') + required_str);
				}					
			}
		},
		showError : function(el) {
			var field_error = $(el).attr('data-valid');
			if (field_error) {
				$('#' + field_error).addClass('show');
			}		
		},
		hideError : function(el) {
			var field_error = $(el).attr('data-valid');
			if (field_error) {
				$('#' + field_error).removeClass('show');
			}		
		},
		seedInputs: function(e) {
			if($(e.target).attr('id') == 'color-select') {
				$(this).next('.color-indicator').empty()
				.append('<img src="' + $(e.target).find('option').not(function(){ return !this.selected }).data("color-image") + '" alt="color preview" />');
			}
			$($(e.target).data('seed-input')).val($(e.target).val());
		}
	}
	mtdpm.controls = {
		init : function() {
			$('.form-checkbox').on('click', mtdpm.controls.checkbox);
			$('.form-unit').on('click', mtdpm.controls.focusField);
			$('.form-input').on('click', mtdpm.controls.focusField);
			$('input').each(mtdpm.controls.placeholders);
		},
		checkbox : function(e) {
			var check = $('input', this);
			if (e.target.type != 'checkbox') { 
				if (check.prop('checked')) {
					check.prop('checked', false);
				}else {
					check.prop('checked', true)
				}
			}			
		},
		placeholders : function() {
			if(this.type != 'checkbox' && this.type != 'radio' && this.type != 'hidden') {
				if (this.hasAttribute('required') && !this.hasAttribute('placeholder')) {
					$(this).attr('placeholder','required')
				} else if (!this.hasAttribute('required') && !this.hasAttribute('placeholder') && !$(this).hasClass('field-phone')) {
					$(this).attr('placeholder','optional')
				}
			}
		},
		focusField : function(e) {
			var el_type = e.target.tagName.toLowerCase();
			if (el_type != 'input' && el_type != 'select' && el_type != 'textarea' && el_type != 'label' && $(this).find('.form-input').size() == 0) { 
				$(e.target).find('input').focus();
				$(e.target).find('select').focus();
				$(e.target).find('textarea').focus();
			}
		}	
	},
	mtdpm.util = {		
		init : function() {
			$('.expand').on('click', mtdpm.util.expand);
			$('.collapse-only').on('click', mtdpm.util.collapseOnly);
			$('.toggle').on('click', mtdpm.util.toggle);	
		},
		expand : function(e) {
			var parent = $(this).parent();
			if ( ! parent.hasClass('expanded'))
			{
				var last = $('.expanded');
				last.find('.plus-r').removeClass('icon-circle-minus').addClass('icon-circle-plus');
				last.removeClass('expanded').addClass('collapsed');
				parent.removeClass('collapsed').addClass('expanded').find('.plus-r').removeClass('icon-circle-plus').addClass('icon-circle-minus');	
			}
			else
			{
				e.preventDefault();
				parent.removeClass('expanded').addClass('collapsed').find('.plus-r').removeClass('icon-circle-minus').addClass('icon-circle-plus');
			}		
		},
		collapseOnly : function(e) {
			e.preventDefault();
			var parent = $(this).parent();
			if ( ! parent.hasClass('expanded'))
			{
				parent.removeClass('collapsed').addClass('expanded').find('.plus-r').removeClass('icon-circle-plus').addClass('icon-circle-minus');
			}else
			{
				parent.removeClass('expanded').addClass('collapsed').find('.plus-r').removeClass('icon-circle-minus').addClass('icon-circle-plus');
			}			
		}, 
		toggle : function(e) {
			e.preventDefault();
			if (! $(this).hasClass('toggle-expanded')) {
				var list = $(this).parent().parent();
				$('a', list).removeClass('toggle-expanded');
				$('.hide', list).removeClass('show');				
			}
			$(this).toggleClass('toggle-expanded');
			var toggle_el = $(this).attr('data-toggle');
			$('#' + toggle_el).toggleClass('show');			
		}
	},
	mtdpm.stores = {		
		init : function() {
			$('.form-checkbox-location').on('click', mtdpm.stores.geocode);
		},	
		geocode : function(e) {
			if($('#location').size() > 0) {	
				var check = $('#location');
				if (e.target.type != 'checkbox') { 
					if (check.prop('checked')) {
						check.prop('checked', false);
					}else {
						check.prop('checked', true);
					}
			 	}
				var form_store = $('#form-store-find');
				if (check.prop('checked')) {
					$('.locate-by-zip-wrapper', form_store).hide();
					$('.loader', form_store).removeClass('hide');
					$('#zip', form_store).prop('required', false).val('');
					$('#origaddress', form_store).prop('required', false).val('');
					$('#origcity', form_store).prop('required', false).val('');
					$('#origstateprovince', form_store).prop('required', false).val('');
					$('button[type="submit"]', form_store).hide();
					navigator.geolocation.getCurrentPosition(function(position) {
						$('#latitude', form_store).val(position.coords.latitude);
						$('#longitude', form_store).val(position.coords.longitude);
						$('.loader', form_store).addClass('hide');
						$('.location-success', form_store).removeClass('hide');
						$('button[type="submit"]', form_store).show();
					}, function(error) {
						$('#latitude', form_store).val('');
						$('#longitude', form_store).val('');
						$('.locate-by-zip-wrapper', form_store).show();
						$('#location', form_store).prop('checked', false);
						$('.loader', form_store).addClass('hide');
						$('.location-success', form_store).addClass('hide');
						$('button[type="submit"]', form_store).show();
						alert('We\'re sorry, your location could not be determined. Please enter your zipcode to find stores in your area.');
					});
				} else {
					$('#latitude', form_store).val('');
					$('#longitude', form_store).val('');
					$('.locate-by-zip-wrapper', form_store).show();
					$('button[type="submit"]', form_store).show();
					$('.loader', form_store).addClass('hide');
					$('.location-success', form_store).addClass('hide');
					$('#zip', form_store).prop('required', true);
					$('#origaddress', form_store).prop('required', true);
					$('#origcity', form_store).prop('required', true);
					$('#origstateprovince', form_store).prop('required', true);
				}
			} 
			else {
				$('#orig-location-fields').remove();
				navigator.geolocation.getCurrentPosition(function(position) {
					$('#latitude', form_store).attr('value', position.coords.latitude);
					$('#longitude', form_store).attr('value', position.coords.longitude);
				}, function(error) {
					alert('We\'re sorry, your location could not be determined. Please enter your zipcode to find stores in your area.');
				});
			}			
		}
	},
	mtdpm.nav = {
		init : function() {
			mtdpm.nav.menu_open = false;
			mtdpm.nav.setup();
			$('#btn-menu').on(touch_event, mtdpm.nav.showMenu);
			if (isAndroidBrowser) { // accomodates bug in Android browser that causes tap or click events not to fire
				$('#btn-menu-back').on('touchstart', mtdpm.nav.hideMenu);
				$('#btn-cart-back').on('touchstart', mtdpm.nav.hideCart);
				$('#btn-continue').on('touchstart', mtdpm.nav.hideCart);
			}else {
				$('#btn-menu-back').on(touch_event, mtdpm.nav.hideMenu);	
				$('#btn-cart-back').on(touch_event, mtdpm.nav.hideCart);	
				$('#btn-continue').on('click', mtdpm.nav.hideCart);		
			}
			$('#btn-cart').on(touch_event, mtdpm.nav.showCart);
			$('.drill-down a').on('click', mtdpm.nav.addActiveClass);
			$('.doc-list a').on('click', mtdpm.nav.addActiveClass);
			$('.menu-list a').on('click', mtdpm.nav.preventGhostTapBubble);
			$(window).on('orientationchange', mtdpm.nav.setup);
		},
		setup : function() {
			if ($('body').attr('id') != 'show-cart') {
				$('#panel-cart').css({ right: '-' + window.innerWidth + 'px' });
			}else {
				$('#panel-content').addClass('panel-hide').removeClass('panel-show');
				$('.cart-lazyload').each(mtdpm.optimize.lazyLoad);
			}
			$('#panel-menu').css({ left: '-' + window.innerWidth + 'px' });
		},
		addActiveClass : function(e) {
			$(this).addClass('active'); 
		},
		preventGhostTapBubble : function() {
			if (! mtdpm.nav.menu_open) { 
				$(this).addClass('ghosttap'); 
			}else {
				$(this).removeClass('ghosttap'); 					
			}
			return mtdpm.nav.menu_open; 			
		},
		showMenu : function(e) {
			$('#panel-menu').addClass('panel-show')
											.removeClass('panel-hide')
											.animate({ translate: window.innerWidth + 'px, 0px' }, duration, 'ease-in-out', function() {
												mtdpm.nav.menu_open = true;
												mtdpm.nav.hideContent();
											});
		},
		hideMenu : function() {
			mtdpm.nav.menu_open = false;
			mtdpm.nav.showContent();
			$('#panel-menu').animate({ translate: '-' + window.innerWidth + 'px, 0px'}, duration, 'ease-in-out', function() {
				$('#panel-menu').addClass('panel-hide').removeClass('panel-show');
			});
		},
		showCart : function(e) {
			var translate = '-' + window.innerWidth;
			if ($('body').attr('id') == 'show-cart') {
				translate = 0;
			}
			$('.cart-lazyload').each(mtdpm.optimize.lazyLoad);
			$('#panel-cart').addClass('panel-show')
											.removeClass('panel-hide')
											.animate({ translate: translate + 'px, 0px' }, duration, 'ease-in-out', function() {
																mtdpm.nav.hideContent();
											});
		},
		hideCart : function() {
			mtdpm.nav.showContent();
			if ($('body').attr('id') == 'show-cart') {
				mtdpm.slider.cycle();
			}
			$('#panel-cart').animate({ translate: window.innerWidth + 'px, 0px' }, duration, 'ease-in-out', function() {
				$('#panel-cart').addClass('panel-hide').removeClass('panel-show');
			});
		},
		showContent : function() {
			$('#panel-content').addClass('panel-show').removeClass('panel-hide');
		},
		hideContent : function() {
			$('#panel-content').addClass('panel-hide').removeClass('panel-show');			
		}
	},
	mtdpm.slider = {
		init : function() {
			var slider = $('#slider');
			if (slider.length > 0) $('.slider-img', slider).each(mtdpm.optimize.lazyLoad);
			if ( Modernizr.csstransforms ) {
				if (slider.length > 0) {
					var sliderChildren = slider.find('li');
					if (sliderChildren.length > 1) {
						var sliderBullets = $('#slider-bullets');
						sliderChildren.each(function(index) {
							var em = $('<em>&middot;</em>');
							if (index == 0) em.addClass('on');
							sliderBullets.append(em);
						});
						mtdpm.slider.cycle();
					}
				}
			}			
		},
		cycle : function() {
			var swipe = new Swipe(document.getElementById('slider'), {
				startSlide: 0,
				speed: 400,
				auto: 5000,
				callback: function(e, pos) {
					var i = bullets.length;
					while (i--) {
						bullets[i].className = '';
					}
					bullets[pos].className = 'on';
				}
			}),
			bullets = document.getElementById('slider-pos').getElementsByTagName('em');							
		}
	}
} (mtdparts.mobile));

$(function() {
	mtdparts.mobile.nav.init();
	mtdparts.mobile.slider.init();
	mtdparts.mobile.optimize.init();
	mtdparts.mobile.util.init();
	mtdparts.mobile.stores.init();
	mtdparts.mobile.cart.init();
	mtdparts.mobile.validate.init();
	mtdparts.mobile.controls.init();
});