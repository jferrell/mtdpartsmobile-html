class ServiceProvider
  
  def self.service_providers
    @service_providers = [
      { 
        :id => 1, 
        :name => "Chilton Turf Center", 
        :address => "607 Craighead Street", 
        :city => "Nashville", :state => "TN", 
        :zip => "37204", 
        :phone => "(615) 254-1637",
        :miles => 4.7
      },
      { 
        :id => 2, 
        :name => "McHenry Tractor Co Inc", 
        :address => "1402 Dickerson Road", 
        :city => "Nashville", 
        :state => "TN", 
        :zip => "37207", 
        :phone => "(615) 227-8291",
        :miles => 7.5
      },
      { 
        :id => 3, 
        :name => "Fixwell Power Equipment Inc", 
        :address => "1228 Northgate Business Parkway", 
        :city => "Madison", 
        :state => "TN", 
        :zip => "37115", 
        :phone => "(615) 865-6608",
        :miles => 15.1
      }
    ]
  end
  
end