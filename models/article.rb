class Article
  
  def self.categories
    @products = { 'maintenance' => { 'name' => 'Maintenance' },
                  'repairs' => { 'name' => 'Repairs' },
                  'safety' => { 'name' => 'Safety' },
                  'tricks-tips' => { 'name' => 'Tricks &amp; Tips' },
                  'spring-seasonal-tips' => { 'name' => 'Spring Seasonal Tips' },
                  'summer-seasonal-tips' => { 'name' => 'Summer Seasonal Tips' },
                  'fall-seasonal-tips' => { 'name' => 'Fall Seasonal Tips' },
                  'winter-seasonal-tips' => { 'name' => 'Winter Seasonal Tips' }
                }
  end
  
end  
