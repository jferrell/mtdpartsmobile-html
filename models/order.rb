class Order
  
  def self.get_order(id)
    @order_numbers = self.orders
    @found_orders = @order_numbers.select {|o| o[:id] == id }
    if @found_orders.length > 0
      @active_order =  @found_orders[0]
    else
      @active_order = @order_numbers[0]
    end
    @active_order
  end
  
  def self.orders
    @order_numbers = [
      { :id => '1234567', :date => 'March 5, 2012', :status => 'Approved', :total => 531.25 },
      { :id => '1234568', :date => 'March 11, 2012', :status => 'Shipped', :total => 431.15 },
      { :id => '1234569', :date => 'March 19, 2012', :status => 'Approved', :total => 123.45 },
      { :id => '12345610', :date => 'March 23, 2012', :status => 'Shipped', :total => 335.72 },
      { :id => '12345611', :date => 'March 28, 2012', :status => 'Approved', :total => 498.03 },
      { :id => '12345612', :date => 'April 2, 2012', :status => 'Shipped', :total => 399.76 }
    ]
  end
  
end