class Products
    @products = { 
      '1419369' => { 
        'name' => 'Yard-Man 21" 139CC Self-Propelled Mower', 
        'item' => '12A-A1M9001', 
        'photo' => 'product-category.jpg', 
        'photo_hd' => 'product-category-hd.jpg', 
        'photo_detail' => 'product-detail.jpg', 
        'photo_detail_hd' => 'product-detail-hd.jpg', 
        'backordered' => true,
        'reg_price' => 269.00,
        'sale_price' => 269.00,
        'is_part' => false 
      },
      '1558828' => { 
        'name' => 'Yard Machines 139CC Tiller', 
        'item' => '21A-24MK052', 
        'photo' => 'product-category2.jpg',  
        'photo_hd' => 'product-category2-hd.jpg', 
        'photo_detail' => 'product-detail2.jpg', 
        'photo_detail_hd' => 'product-detail2-hd.jpg', 
        'backordered' => false, 
        'reg_price' => 329.99,
        'sale_price' => 309.99,
        'is_part' => false  
      },
      '1558830' => { 
        'name' => 'Yard Machines 30" 357CC Two-Stage Snow Thrower', 
        'item' => '31AH65FH700', 
        'photo' => 'product-category3.jpg', 
        'photo_hd' => 'product-category3-hd.jpg', 
        'photo_detail' => 'product-detail3.jpg', 
        'photo_detail_hd' => 'product-detail3-hd.jpg', 
        'backordered' => false,
        'reg_price' => 999.00,
        'sale_price' => 999.00,
        'is_part' => false 
      },
      '1410819' => { 
        'name' => 'Remington RM170B Cordless Blower', 
        'item' => '41AA170G983', 
        'photo' => 'product-category4.jpg', 
        'photo_hd' => 'product-category4-hd.jpg', 
        'photo_detail' => 'product-detail4.jpg', 
        'photo_detail_hd' => 'product-detail4-hd.jpg', 
        'backordered' => false,
        'reg_price' => 49.99,
        'sale_price' => 49.99,
        'is_part' => false   
      },
      '329744' => { 
        'name' => 'Edger Belt 0.218 x 35.80', 
        'item' => '954-04032B', 
        'photo' => 'part-category.jpg', 
        'photo_hd' => 'part-category-hd.jpg', 
        'photo_detail' => 'part-detail.jpg', 
        'photo_detail_hd' => 'part-detail-hd.jpg', 
        'backordered' => false,
        'reg_price' => 12.33,
        'sale_price' => 12.33,
        'is_part' => true  
      }
    } 
  
  @categories = { 'chainsaws' => 'Chainsaws', 'chippers-shredders-vacuums' => 'Chippers, Shredders &amp; Vacuums', 'garden-tillers' => 'Garden Tillers', 'lawn-mowers-walk-behind' => 'Lawn Mowers: Walk Behind', 'lawn-mowers-tractors-riding' => 'Lawn Mowers &amp; Tractors: Riding', 'lawn-string-trimmers' => 'Lawn / String Trimmers', 'snow-blowers-throwers' => 'Snow Blowers / Throwers', 'universal-misc' => 'Universal &amp; Misc' } 
    
  def self.get_all()
    @products
  end
  
  def self.get(id)
    @products[id]
  end

  def self.get_all_categories()
    @categories
  end
  
  def self.get_category(category)
    @categories[category]
  end
end