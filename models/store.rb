class Store
  
  def self.stores
    @stores = [
      { :id => 1, :name => "PRO-TECH LAWNMOWER SALES & SERVICE", :address => "27125 NORTH SIERRA HIGHWAY 402", :city => "SANTA CLARITA", :state => "CA", :zip => "91351", :phone => "(661) 298-7233", :latitude => "34.410021", :longitude => "-118.459073" },
      { :id => 2, :name => "D&M LAWN AND GARDEN, INC.", :address => "2801 EAST MIRALOMA AVENUE", :city => "ANAHEIM", :state => "CA", :zip => "92806", :phone => "(714) 996-5490", :latitude => "33.855015", :longitude => "-117.872091" },
      { :id => 3, :name => "LITTLE ENGINES MOWER", :address => "147 WEST HIGHWAY 246", :city => "BUELLTON", :state => "CA", :zip => "93427", :phone => "(805) 688-8573", :latitude => "34.61445", :longitude => "-120.19606" }
    ]
  end
  
end